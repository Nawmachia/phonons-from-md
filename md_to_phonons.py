#!/usr/bin/env python3
################################################################
#
# Analyse CASTEP md trajectory file to extract phonon info
#
################################################################

import argparse
import sys
import numpy as np
import scipy as sp
from typing import List

def parse_cli_flags(argv: List[str]) -> argparse.Namespace:
    parser = argparse.ArgumentParser(
        description="Analyse a CASTEP molecular dynamics trajectory file to extract phonon information",
        epilog="Example usage: ./md_to_phonons -o configuration.out mdinput.md",
    )

    parser.add_argument(
        "md_file", type=str, nargs="?",
        help="CASTEP molecular dynamics trajectory file",
    )
    
    parser.add_argument(
        "-o", "--output",type=str, required=False,
        help="Output file to write, Default is to standard output",
        default="-",
    )
    return parser.parse_args(argv)


def main(argv: List[str]) -> None:
    import castep_md_reader as md

    arguments = parse_cli_flags(argv)    #Use this version of parse
    input_filename  = arguments.md_file  #not the one in castep_md_reader\
    output_filename = arguments.output
    
    #Read a CASTEP MD file and store info in the list
    configuration = md.read_md_file(input_filename)
    if output_filename == "-" or output_filename is None:
        print ('success - read configuration OK from ',input_filename)
    else:
        with open(output_filename, "w") as file:
            file.write('success - read configuration OK')

    #Nawshad code follows:
    
    # store data here then output

    # Number of configurations
    n_configurations = len(configuration)
    print ('Number of configurations are: ', n_configurations)
    
    # Number of atoms in each configurations
    n_atoms = 0
    for _, t in configuration[0].body.items():
        n_atoms += len(t)
    print ('Number of atoms are :', n_atoms)
        
    # Set up a 3D array with correct sizes
    velocities = np.zeros ((3, n_atoms, n_configurations), dtype=float)
        
    for i in range (0, n_configurations, 1):

        atom_value = 0

        # Assign value of velocities from configurations to velocities array
        for _, value in configuration[i].body.items(): # Set of velocity data from md file

            for atom in value:
                
                velocities[0,atom_value,i] = (atom.v_x)
                velocities[1,atom_value,i] = (atom.v_y)
                velocities[2,atom_value,i] = (atom.v_z)
                
                atom_value = atom_value + 1
                
    print (velocities)



if __name__ == "__main__":
    main(sys.argv[1:])

